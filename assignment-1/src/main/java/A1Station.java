/**
 * A1Station.java
 * Train to Javari Park
 *
 * Created by William Gates (1706040044) on 3/3/18.
 * Copyright (c) 2018 William Gates. All rights reserved.
 */

import java.util.Scanner;

public class A1Station {
    // Mendeklarasikan sebuah variabel THRESHOLD sebagai pembatas berat (dalam kilogram) yang
    // diizinkan dalam satu kereta.
    private static final double THRESHOLD = 250;
    // Mendeklarasikan sebuah array carList untuk menampung semua objek TrainCar yang dibuat.
    public static TrainCar[] carList;

    // Menghitung nilai rata-rata mass index.
    public static double averageMassIndex(int i) {
        return carList[i].computeTotalMassIndex() / (i + 1);
    }

    // Mengelompokkan nilai rata-rata mass index.
    public static String massIndexCategory(int i) {
        double average = averageMassIndex(i);
        if (average < 18.5) {
            return "*underweight*";
        }
        else if (average >= 18.5 && average < 25.0) {
            return "*normal*";
        }
        else if (average >= 25.0 && average < 30.0) {
            return "*overweight*";
        }
        else {
            return "*obese*";
        }
    }

    // Main.
    public static void main(String[] args) {
        // Menginisialisasi variabel dan menerima nilai berupa jumlah kucing yang akan diinput.
        Scanner input = new Scanner(System.in);
        int numberOfCats = Integer.parseInt(input.nextLine());
        carList = new TrainCar[numberOfCats];

        // Melakukan pengulangan hingga semua kucing berangkat.
        for (int i = 0; i < numberOfCats; i++) {
            String[] catInput = input.nextLine().split(",");

            // Membuat objek TrainCar pertama.
            if (i == 0) {
                carList[i] = new TrainCar(new WildCat(catInput[0], Double.parseDouble(catInput[1]),
                                          Double.parseDouble(catInput[2])));
            }
            // Membuat objek TrainCar berikutnya yang memiliki reference ke TrainCar sebelumnya.
            else {
                carList[i] = new TrainCar(new WildCat(catInput[0], Double.parseDouble(catInput[1]),
                                          Double.parseDouble(catInput[2])), carList[i - 1]);
            }

            // Memberangkatkan kereta apabila kucing dalam kereta sudah melebihi THRESHOLD atau
            // kucing yang akan diinput sudah habis.
            if (carList[i].computeTotalWeight() > THRESHOLD || i == numberOfCats - 1) {
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                carList[i].printCar();
                System.out.println("Average mass index of all cats: "
                                   + String.format("%.2f", averageMassIndex(i)));
                System.out.println("In average, the cats in the train are "
                                   + massIndexCategory(i));

                carList = new TrainCar[numberOfCats - i - 1];
                numberOfCats = numberOfCats - i - 1;
                i = -1;
            }
        }
    }
}
