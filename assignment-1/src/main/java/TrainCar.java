/**
 * TrainCar.java
 * Train to Javari Park
 *
 * Created by William Gates (1706040044) on 3/3/18.
 * Copyright (c) 2018 William Gates. All rights reserved.
 */

public class TrainCar {
    // Mendeklarasikan sebuah variabel EMPTY_WEIGHT yang merupakan berat (dalam kilogram) dari
    // kereta.
    public static final double EMPTY_WEIGHT = 20;

    // Mendeklarasikan instance variable.
    WildCat cat;
    TrainCar next;

    // Membuat constructor yang tidak memiliki reference terhadap TrainCar berikutnya.
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    // Membuat constructor yang memiliki reference terhadap TrainCar berikutnya.
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // Menghitung berat total kereta termasuk kucing.
    public double computeTotalWeight() {
        if (next == null) {
            return EMPTY_WEIGHT + cat.weight;
        }
        else {
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }
    }

    // Menghitung total mass index.
    public double computeTotalMassIndex() {
        if (next == null) {
            return cat.computeMassIndex();
        }
        else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    // Menampilkan kucing dalam kereta yang akan berangkat.
    public void printCar() {
        if (next == null) {
            System.out.print("(" + cat.name + ")" + "\n");
        }
        else {
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
