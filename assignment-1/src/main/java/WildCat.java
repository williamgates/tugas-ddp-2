/**
 * WildCat.java
 * Train to Javari Park
 *
 * Created by William Gates (1706040044) on 3/3/18.
 * Copyright (c) 2018 William Gates. All rights reserved.
 */

public class WildCat {
    // Mendeklarasikan variabel nama kucing, berat kucing dalam kilogram, dan panjang kucing dalam
    // centimeter.
    String name;
    double weight;
    double length;

    // Membuat sebuah constructor dari WildCat.
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // Menghitung mass index dari kucing.
    public double computeMassIndex() {
        double bmi = weight / ((length / 100.0) * (length / 100.0));
        return bmi;
    }
}
