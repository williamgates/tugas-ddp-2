package cage;

import cage.Cage;
import java.util.ArrayList;

public class Arrangement {
    // Method arrangeCages berfungsi untuk mengurutkan kandang.
    public static Cage[][] arrangeCages(Cage[] listOfCages) {
        // Menjalankan method apabila banyak kandang yang akan diurutkan lebih dari 0.
        if (listOfCages.length > 0) {
            // Mendeklarasikan variable.
            int index = 0;
            int[] level = new int[3];
            Cage[][] arrangedCages = new Cage[3][];
            ArrayList<Cage> arrangedPerLevel = new ArrayList<Cage>();

            System.out.println("location: " + listOfCages[0].getLocation());

            // Menentukan banyak kandang pada setiap level.
            if (listOfCages.length >= 3) {
                level[0] = listOfCages.length / 3;
                level[1] = level[0];
                level[2] = level[0];
                if (listOfCages.length % 3 != 0) {
                    level[2] = level[0] + listOfCages.length % 3;
                }
            }
            else if (listOfCages.length == 2){
                level[0] = 1;
                level[1] = 1;
                level[2] = 0;
            }
            else {
                level[0] = 1;
                level[1] = 0;
                level[2] = 0;
            }

            // Memasukkan kandang pada setiap level sesuai urutan.
            for (int i = 0; i < level.length; i++) {
                for (int j = 0; j < level[i]; j++) {
                    arrangedPerLevel.add(listOfCages[index++]);
                }
                arrangedCages[i] = arrangedPerLevel.toArray(new Cage[arrangedPerLevel.size()]);
                arrangedPerLevel = new ArrayList<Cage>();
            }

            // Menampilkan urutan kandang pada setiap level.
            for (int i = level.length - 1; i >= 0; i--) {
                System.out.print("level " + (i + 1) + ":");
                for (int j = 0; j < level[i]; j++) {
                    System.out.print(" " + arrangedCages[i][j].getOwner() + " (" +
                                     arrangedCages[i][j].getAnimalSize() + " - " +
                                     arrangedCages[i][j].getCageSize() + "),");
                }
                System.out.println();
            }
            System.out.println();
            return arrangedCages;
        }
        return null;
    }

    // Method rearrangeCages berungsi untuk mengurutkan kembali kandang.
    public static void rearrangeCages(Cage[][] listOfCages) {
        // Menjalankan method apabila ada kandang yang akan diurutkan.
        if (listOfCages != null) {
            System.out.println("After rearrangement...");

            // Menampilkan urutan kandang level 3 yang sudah diurutkan kembali.
            System.out.print("level 3:");
            for (int i = listOfCages[1].length - 1; i >= 0 ; i--) {
                System.out.print(" " + listOfCages[1][i].getOwner() + " (" +
                                 listOfCages[1][i].getAnimalSize() + " - " +
                                 listOfCages[1][i].getCageSize() + "),");
            }
            System.out.println();

            // Menampilkan urutan kandang level 2 yang sudah diurutkan kembali.
            System.out.print("level 2:");
            for (int i = listOfCages[0].length - 1; i >= 0 ; i--) {
                System.out.print(" " + listOfCages[0][i].getOwner() + " (" +
                                 listOfCages[0][i].getAnimalSize() + " - " +
                                 listOfCages[0][i].getCageSize() + "),");
            }
            System.out.println();

            // Menampilkan urutan kandang level 1 yang sudah diurutkan kemabli.
            System.out.print("level 1:");
            for (int i = listOfCages[2].length - 1; i >= 0 ; i--) {
                System.out.print(" " + listOfCages[2][i].getOwner() + " (" +
                                 listOfCages[2][i].getAnimalSize() + " - " + listOfCages[2][i].getCageSize() + "),");
            }
            System.out.println("\n");
        }
    }
}
