package cage;

public class Cage {
    // Mendeklarasikan instance variable dari Cage.
    private int animalSize;
    private String cageSize;
    private String cageType;
    private String location;
    private String owner;

    // Constructor dari class Cage.
    public Cage(int size, String name, String animalType) {
        owner = name;
        animalSize = size;

        // Menentukan kandang yang cocok untuk hewan yang akan dimasukkan.
        if (animalType.equals("pet")) {
            location = "indoor";
            if (size < 45) {
                cageSize = "A";
            }
            else if (size < 60) {
                cageSize = "B";
            }
            else {
                cageSize = "C";
            }
        }
        else if (animalType.equals("wild")) {
            location = "outdoor";
            if (size < 75) {
                cageSize = "A";
            }
            else if (size < 90) {
                cageSize = "B";
            }
            else {
                cageSize = "C";
            }
        }
    }

    // Method getter dari class Cage.
    public int getAnimalSize() {
        return animalSize;
    }

    public String getCageSize() {
        return cageSize;
    }

    public String getLocation() {
        return location;
    }

    public String getOwner() {
        return owner;
    }
}
