package animals;

import cage.Cage;

public class Eagle {
    // Mendeklarasikan instance variable.
    private int size;
    private String animalType = "wild";
    private String name;
    private Cage cage;

    // Constructor dari class Eagle.
    public Eagle(int size, String name) {
        this.size = size;
        this.name = name;
        this.cage = new Cage(size, name, animalType);
    }

    // Method orderFly berfungsi untuk menjalankan perintah ketika elang disuruh untuk terbang.
    public void orderFly() {
        System.out.println(name + " makes a voice: kwaakk...");
        System.out.println("You hurt!");
    }

    // Method getter dari class Eagle.
    public String getName() {
        return name;
    }

    public Cage getCage() {
        return cage;
    }
}
