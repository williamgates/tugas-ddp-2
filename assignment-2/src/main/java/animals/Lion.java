package animals;

import cage.Cage;

public class Lion {
    // Mendeklarasikan instance variable.
    private int size;
    private String animalType = "wild";
    private String name;
    private Cage cage;

    // Constructor dari class Lion.
    public Lion(int size, String name) {
        this.size = size;
        this.name = name;
        this.cage = new Cage(size, name, animalType);
    }

    // Method seeHunting berfungsi untuk menjalankan perintah melihat singa yang sedang berburu.
    public void seeHunting() {
        System.out.println("Lion is hunting..");
        System.out.println(name + " makes a voice: err...!");
    }

    // Method brushMane berfungsi untuk menjalankan perintah ketika singa dibersihkan.
    public void brushMane() {
        System.out.println("Clean the lion’s mane..");
        System.out.println(name + " makes a voice: Hauhhmm!");
    }

    // Method disturb berfungsi untuk menjalankan perintah ketika singa diganggu.
    public void disturb() {
        System.out.println(name + " makes a voice: HAUHHMM!");
    }

    // Method getter dari class Lion.
    public String getName() {
        return name;
    }

    public Cage getCage() {
        return cage;
    }
}
