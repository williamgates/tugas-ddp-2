package animals;

import cage.Cage;
import java.util.Scanner;

public class Parrot {
    // Mendeklarasikan instance variable.
    private Scanner input = new Scanner(System.in);
    private int size;
    private String animalType = "pet";
    private String name;
    private Cage cage;

    // Constructor dari class Parrot.
    public Parrot(int size, String name) {
        this.size = size;
        this.name = name;
        this.cage = new Cage(size, name, animalType);
    }

    // Method orderFly berfungsi untuk menjalankan perintah ketika burung beo disuruh untuk terbang.
    public void orderFly() {
        System.out.println("Parrot " + name + " flies!");
        System.out.println(name + " makes a voice: FLYYYY.....");
    }

    // Method doConversation berfungsi untuk menjalankan perintah melakukan percakapan dengna burung
    // beo.
    public void doConversation() {
        System.out.print("You say: ");
        String say = input.nextLine();
        System.out.println(name + " says: " + say.toUpperCase());
    }

    // Method otherCommand berfungsi untuk menjalankan perintah yang tidak dikenal burung beo.
    public void otherCommand() {
        System.out.println(name + " says: HM?");
    }

    // Method getter dari class Parrot.
    public String getName() {
        return name;
    }

    public Cage getCage() {
        return cage;
    }
}
