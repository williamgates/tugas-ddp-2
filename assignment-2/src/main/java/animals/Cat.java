package animals;

import cage.Cage;
import java.util.Random;

public class Cat {
    // Mendeklarasikan instance variable.
    Random rand = new Random();
    private int size;
    private String animalType = "pet";
    private String name;
    private Cage cage;

    // Constructor dari class Cat.
    public Cat(int size, String name) {
        this.size = size;
        this.name = name;
        this.cage = new Cage(size, name, animalType);
    }

    // Method brushFur berfungsi untuk menjalankan perintah ketika kucing dibersihkan.
    public void brushFur() {
        System.out.println("Time to clean " + name + "'s fur");
        System.out.println(name + " makes a voice: Nyaaan...");
    }

    // Method cuddle berfungsi untuk menjalankan perintah ketika kucing dipeluk.
    public void cuddle() {
        int n = rand.nextInt(5);
        String voices[] = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        System.out.println(name + " makes a voice: " + voices[n]);
    }

    // Method getter dari class Cat.
    public String getName() {
        return name;
    }

    public Cage getCage() {
        return cage;
    }
}
