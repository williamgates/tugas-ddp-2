package animals;

import cage.Cage;

public class Hamster {
    // Mendeklarasikan instance variable.
    private int size;
    private String animalType = "pet";
    private String name;
    private Cage cage;

    // Constructor dari class Hamster.
    public Hamster(int size, String name) {
        this.size = size;
        this.name = name;
        this.cage = new Cage(size, name, animalType);
    }

    // Method seeGnaw berfungsi untuk menjalankan perintah melihat hamster yang sedang menggerogot.
    public void seeGnaw() {
        System.out.println(name + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    // Metod orderRun berfungsi untuk menjalankan perintah ketika hamster disuruh untuk berlari.
    public void orderRun() {
        System.out.println(name + " makes a voice: trrr.... trrr...");
    }

    // Method getter dari class Hamster.
    public String getName() {
        return name;
    }

    public Cage getCage() {
        return cage;
    }
}
