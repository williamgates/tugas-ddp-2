import animals.*;
import cage.*;
import java.util.Scanner;

public class JavariPark {
    public static void main(String[] args) {
        // Mendeklarasikan variable.
        Scanner input = new Scanner(System.in);
        boolean found = false;
        int intInput = 0;
        String stringInput = "";
        String animals[] = {"cat", "eagle", "hamster", "parrot", "lion"};
        Cage[][] arrangedCatsCages;
        Cage[][] arrangedLionsCages;
        Cage[][] arrangedEaglesCages;
        Cage[][] arrangedParrotsCages;
        Cage[][] arrangedHamstersCages;

        // Menampilkan selamat datang dan meminta input dari user.
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        try {
            // Meminta input dari user berupa jumlah kucing dan data dari kucing untuk membuat objek
            // dari setiap kucing.
            System.out.print("cat: ");
            int numberOfCats = Integer.parseInt(input.nextLine());
            Cat[] listOfCats = new Cat[numberOfCats];
            Cage[] catsCages = new Cage[numberOfCats];

            if (numberOfCats > 0) {
                System.out.println("Provide the information of cat(s):");
                String[] catsData = input.nextLine().split(",");
                for (int i = 0; i < numberOfCats; i++) {
                    listOfCats[i] = new Cat(Integer.parseInt(catsData[i].split("\\|")[1]),
                                            catsData[i].split("\\|")[0]);
                    catsCages[i] = listOfCats[i].getCage();
                }
            }

            // Meminta input dari user berupa jumlah singa dan data dari singa untuk membuat objek
            // dari setiap singa.
            System.out.print("lion: ");
            int numberOfLions = Integer.parseInt(input.nextLine());
            Lion[] listOfLions = new Lion[numberOfLions];
            Cage[] lionsCages = new Cage[numberOfLions];

            if (numberOfLions > 0) {
                System.out.println("Provide the information of lion(s):");
                String[] lionsData = input.nextLine().split(",");
                for (int i = 0; i < numberOfLions; i++) {
                    listOfLions[i] = new Lion(Integer.parseInt(lionsData[i].split("\\|")[1]),
                                              lionsData[i].split("\\|")[0]);
                    lionsCages[i] = listOfLions[i].getCage();
                }
            }

            // Meminta input dari user berupa jumlah elang dan data dari elang untuk membuat objek
            // dari setiap elang.
            System.out.print("eagle: ");
            int numberOfEagles = Integer.parseInt(input.nextLine());
            Eagle[] listOfEagles = new Eagle[numberOfEagles];
            Cage[] eaglesCages = new Cage[numberOfEagles];

            if (numberOfEagles > 0) {
                System.out.println("Provide the information of eagle(s):");
                String[] eaglesData = input.nextLine().split(",");
                for (int i = 0; i < numberOfEagles; i++) {
                    listOfEagles[i] = new Eagle(Integer.parseInt(eaglesData[i].split("\\|")[1]),
                                                eaglesData[i].split("\\|")[0]);
                    eaglesCages[i] = listOfEagles[i].getCage();
                }
            }

            // Meminta input dari user berupa jumlah burung beo dan data dari burung beo untuk
            // membuat objek dari setiap burung beo.
            System.out.print("parrot: ");
            int numberOfParrots = Integer.parseInt(input.nextLine());
            Parrot[] listOfParrots = new Parrot[numberOfParrots];
            Cage[] parrotsCages = new Cage[numberOfParrots];

            if (numberOfParrots > 0) {
                System.out.println("Provide the information of parrot(s):");
                String[] parrotsData = input.nextLine().split(",");
                for (int i = 0; i < numberOfParrots; i++) {
                    listOfParrots[i] = new Parrot(Integer.parseInt(parrotsData[i].split("\\|")[1]),
                                                  parrotsData[i].split("\\|")[0]);
                    parrotsCages[i] = listOfParrots[i].getCage();
                }
            }

            // Meminta input dari user berupa jumlah hamster dan data dari hamster untuk membuat
            // objek dari setiap hamster.
            System.out.print("hamster: ");
            int numberOfHamsters = Integer.parseInt(input.nextLine());
            Hamster[] listOfHamsters = new Hamster[numberOfHamsters];
            Cage[] hamstersCages = new Cage[numberOfHamsters];

            if (numberOfHamsters > 0) {
                System.out.println("Provide the information of hamster(s):");
                String[] hamstersData = input.nextLine().split(",");
                for (int i = 0; i < numberOfHamsters; i++) {
                    listOfHamsters[i] = new Hamster(Integer.parseInt(hamstersData[i].split("\\|")[1]),
                                                    hamstersData[i].split("\\|")[0]);
                    hamstersCages[i] = listOfHamsters[i].getCage();
                }
            }

            // Mengurutkan setiap kandang yang ada pada Javari Park.
            System.out.println("Animals have been successfully recorded!");
            System.out.println();
            System.out.println("=============================================");
            System.out.println("Cage arrangement:");

            arrangedCatsCages = Arrangement.arrangeCages(catsCages);
            Arrangement.rearrangeCages(arrangedCatsCages);
            arrangedLionsCages = Arrangement.arrangeCages(lionsCages);
            Arrangement.rearrangeCages(arrangedLionsCages);
            arrangedEaglesCages = Arrangement.arrangeCages(eaglesCages);
            Arrangement.rearrangeCages(arrangedEaglesCages);
            arrangedParrotsCages = Arrangement.arrangeCages(parrotsCages);
            Arrangement.rearrangeCages(arrangedParrotsCages);
            arrangedHamstersCages = Arrangement.arrangeCages(hamstersCages);
            Arrangement.rearrangeCages(arrangedHamstersCages);

            // Menampilkan jumlah hewan setiap jenisnya.
            System.out.println("NUMBER OF ANIMALS:");
            System.out.println("cat:" + listOfCats.length);
            System.out.println("lion:" + listOfLions.length);
            System.out.println("parrot:" + listOfParrots.length);
            System.out.println("eagle:" + listOfEagles.length);
            System.out.println("hamster:" + listOfHamsters.length);
            System.out.println();
            System.out.println("=============================================");

            // Melakukan pengulangan untuk meminta input dari user berupa apa yang akan dilakukan
            // di Javari Park.
            while (true) {
                // Menampilkan opsi jenis hewan yang ada beserta exit dan meminta input dari user.
                System.out.println("Which animal you want to visit?");
                System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
                intInput = Integer.parseInt(input.nextLine());
                if (intInput == 99) {
                    break;
                }

                // Meminta input dari user berupa nama dari hewan yang akan dikunjungi.
                System.out.print("Mention the name of " + animals[intInput - 1] +
                                 " you want to visit: ");
                stringInput = input.nextLine();

                // Mencari nama hewan yang akan dikunjungi pada daftar hewan sesuai dengan jenisnya.
                switch (intInput) {
                    // Apabila nama hewan yang akan dicari adalah kucing.
                    case 1:
                        for (int i = 0; i < listOfCats.length; i++) {
                            if (stringInput.equals(listOfCats[i].getName())) {
                                found = true;

                                // Meminta input dari user berupa apa yang hendak dilakukan terhadap
                                // kucing tersebut.
                                System.out.println("You are visiting " + stringInput +
                                                   " (cat) now, what would you like to do?");
                                System.out.println("1: Brush the fur 2: Cuddle");
                                intInput = Integer.parseInt(input.nextLine());
                                switch (intInput) {
                                    case 1:
                                        listOfCats[i].brushFur();
                                        break;
                                    case 2:
                                        listOfCats[i].cuddle();
                                        break;
                                    default:
                                        System.out.println("You do nothing...");
                                }
                            }
                        }
                        break;

                    // Apabila nama hewan yang akan dicari adalah elang.
                    case 2:
                        for (int i = 0; i < listOfEagles.length; i++) {
                            if (stringInput.equals(listOfEagles[i].getName())) {
                                found = true;

                                // Meminta input dari user berupa apa yang hendak dilakukan terhadap
                                // elang tersebut.
                                System.out.println("You are visiting " + stringInput +
                                                   " (eagle) now, what would you like to do?");
                                System.out.println("1: Order to fly");
                                intInput = Integer.parseInt(input.nextLine());
                                switch (intInput) {
                                    case 1:
                                        listOfEagles[i].orderFly();
                                        break;
                                    default:
                                        System.out.println("You do nothing...");
                                }
                            }
                        }
                        break;

                    // Apabila nama hewan yang akan dicari adalah hamster.
                    case 3:
                        for (int i = 0; i < listOfHamsters.length; i++) {
                            if (stringInput.equals(listOfHamsters[i].getName())) {
                                found = true;

                                // Meminta input dari user berupa apa yang hendak dilakukan terhadap
                                // hamster tersebut.
                                System.out.println("You are visiting " + stringInput +
                                                   " (hamster) now, what would you like to do?");
                                System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                                intInput = Integer.parseInt(input.nextLine());
                                switch (intInput) {
                                    case 1:
                                        listOfHamsters[i].seeGnaw();
                                        break;
                                    case 2:
                                        listOfHamsters[i].orderRun();
                                        break;
                                    default:
                                        System.out.println("You do nothing...");
                                }
                            }
                        }
                        break;

                    // Apabila nama hewan yang akan dicari adalah burung beo.
                    case 4:
                        for (int i = 0; i < listOfParrots.length; i++) {
                            if (stringInput.equals(listOfParrots[i].getName())) {
                                found = true;

                                // Meminta input dari user berupa apa yang hendak dilakukan terhadap
                                // burung beo tersebut.
                                System.out.println("You are visiting " + stringInput +
                                                   " (parrot) now, what would you like to do?");
                                System.out.println("1: Order to fly 2: Do conversation");
                                intInput = Integer.parseInt(input.nextLine());
                                switch (intInput) {
                                    case 1:
                                        listOfParrots[i].orderFly();
                                        break;
                                    case 2:
                                        listOfParrots[i].doConversation();
                                        break;
                                    default:
                                        listOfParrots[i].otherCommand();
                                }
                            }
                        }
                        break;

                    // Apabila nama hewan yang akan dicari adalah singa.
                    case 5:
                        for (int i = 0; i < listOfLions.length; i++) {
                            if (stringInput.equals(listOfLions[i].getName())) {
                                found = true;

                                // Meminta input dari user berupa apa yang hendak dilakukan terhadap
                                // singa tersebut.
                                System.out.println("You are visiting " + stringInput +
                                                   " (lion) now, what would you like to do?");
                                System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                                intInput = Integer.parseInt(input.nextLine());
                                switch (intInput) {
                                    case 1:
                                        listOfLions[i].seeHunting();
                                        break;
                                    case 2:
                                        listOfLions[i].brushMane();
                                        break;
                                    case 3:
                                        listOfLions[i].disturb();
                                        break;
                                    default:
                                        System.out.println("You do nothing...");
                                }
                            }
                        }
                        break;
                }

                // Apabila nama hewan yang dicari tidak ditemukan.
                if (found == false) {
                    System.out.print("There is no " + animals[intInput - 1] + " with that name! ");
                }
                found = false;
                System.out.println("Back to the office!");
                System.out.println();
            }
        }
        // Menangkap input error dari user.
        catch (Exception e) {
            System.out.println("There's something wrong with your input.");
        }
    }
}
