package javari.animal;

/**
 * This class represents common attributes and behaviours found in all animals
 * in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author William Gates
 */
public class Animal {

    private Integer id;
    private String type;
    private String name;
    private Gender gender;
    private Body body;
    private String specialStatus;
    private Condition condition;

    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param specialStatus status of the animal
     * @param condition     health condition of the animal
     */
    public Animal(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.gender = gender;
        this.body = new Body(length, weight, gender);
        this.specialStatus = specialStatus;
        this.condition = condition;
    }

    /**
     * Returns the unique ID that associated with animals in Javari Park.
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Returns the type that associated with the animal.
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the name that associated with the animal.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns {@code Gender} identification of the animal.
     *
     * @return
     */
    public Gender getGender() {
        return body.getGender();
    }

    /**
     * Returns the length that associated with the animal.
     *
     * @return
     */
    public double getLength() {
        return body.getLength();
    }

    /**
     * Returns the width that associated with the animal.
     *
     * @return
     */
    public double getWeight() {
        return body.getWeight();
    }

    /**
     * Returns {@code Condition} of the animal.
     *
     * @return
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Determines whether the animal can perform their attraction or not.
     *
     * @return
     */
    public boolean isShowable() {
        return condition == Condition.HEALTHY && specificCondition();
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return
     */
    protected boolean specificCondition() {
        boolean isPerformable = true;

        if (((type.equals("Lion") || type.equals("Whale") || type.equals("Cat") ||
              type.equals("Hamster")) && specialStatus.equals("pregnant")) ||
            ((type.equals("Eagle") || type.equals("Parrot")) && specialStatus.equals("laying eggs")) ||
            (type.equals("Lion") && (gender == Gender.FEMALE)) ||
            (type.equals("Snake") && !specialStatus.equals("tame"))) {
            isPerformable = false;
        }
        return isPerformable;
    }
}
