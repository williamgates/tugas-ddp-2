package javari.menu;

/**
 * This class prints the input guide menu for visitor.
 *
 * @author William Gates
 */
public class Menu {
    public static String list[] = {". Circles of Fires\n", ". Dancing Animals\n", ". Counting Masters\n", ". Passionate Coders\n"};

    /**
     * Print the Javari Park sections.
     *
     * @return
     */
    public static void printMainMenu() {
        System.out.println("Javari Park has 3 sections");
        System.out.println("1. Explore the Mammals");
        System.out.println("2. World of Aves");
        System.out.println("3. Reptilian Kingdom");
        System.out.print("Please choose your preferred section (type the number): ");
    }

    /**
     * Print the list of mammals.
     *
     * @return
     */
    public static void printMammalsList() {
        System.out.println("--Explore the Mammals--");
        System.out.println("1. Hamster");
        System.out.println("2. Lion");
        System.out.println("3. Cat");
        System.out.println("4. Whale");
        System.out.print("Please choose your preferred animals (type the number): ");
    }

    /**
     * Print the list of aves.
     *
     * @return
     */
    public static void printAvesList() {
        System.out.println("--World of Aves--");
        System.out.println("1. Eagle");
        System.out.println("2. Parrot");
        System.out.print("Please choose your preferred animals (type the number): ");
    }

    /**
     * Print the list of reptiles.
     *
     * @return
     */
    public static void printReptilesList() {
        System.out.println("--Reptilian Kingdom--");
        System.out.println("1. Snake");
        System.out.print("Please choose your preferred animals (type the number): ");
    }

    /**
     * Print the list of hamster's attractions.
     *
     * @return
     */
    public static void printHamsterAttractions() {
        System.out.println("--Hamster--");
        System.out.printf("1%s2%s3%s", list[1], list[2], list[3]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }

    /**
     * Print the list of lion's attractions.
     *
     * @return
     */
    public static void printLionAttractions() {
        System.out.println("--Lion--");
        System.out.printf("1%s", list[0]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }

    /**
     * Print the list of cat's attractions.
     *
     * @return
     */
    public static void printCatAttractions() {
        System.out.println("--Cat--");
        System.out.printf("1%s2%s", list[1], list[3]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }

    /**
     * Print the list of whale's attractions.
     *
     * @return
     */
    public static void printWhaleAttractions() {
        System.out.println("--Whale--");
        System.out.printf("1%s2%s", list[0], list[2]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }

    /**
     * Print the list of eagle's attractions.
     *
     * @return
     */
    public static void printEagleAttractions() {
        System.out.println("--Eagle--");
        System.out.printf("1%s", list[0]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }

    /**
     * Print the list of parrot's attractions.
     *
     * @return
     */
    public static void printParrotAttractions() {
        System.out.println("--Parrot--");
        System.out.printf("1%s2%s", list[1], list[2]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }

    /**
     * Print the list of snake's attractions.
     *
     * @return
     */
    public static void printSnakeAttractions() {
        System.out.println("--Snake--");
        System.out.printf("1%s2%s", list[1], list[3]);
        System.out.print("Please choose your preferred attractions (type the number): ");
    }
}
