package javari.park;

import java.util.List;

import javari.animal.Animal;

/**
 * This interface describes expected behaviours for any type ((abstract)
 * class, interface) that represents the concept of attraction that
 * will be watched by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author William Gates
 */
interface SelectedAttractionInterface {

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    String getName();

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return
     */
    String getType();

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    List<Animal> getPerformers();
}

public class SelectedAttraction implements SelectedAttractionInterface {
    private String name;
    private String type;
    private List<Animal> performers;

    public SelectedAttraction(String name, String type, List<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Animal> getPerformers() {
        return performers;
    }
}
