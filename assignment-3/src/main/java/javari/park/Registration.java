package javari.park;

import java.util.List;
import java.util.ArrayList;

/**
 * This interfaces describes expected behaviours for any type ((abstract)
 * class, interface) that represents the concept of attraction registration
 * done by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author William Gates
 */
interface RegistrationInterface {

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return
     */
    int getRegistrationId();

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return
     */
    String getVisitorName();

    /**
     * Changes visitor's name in the registration.
     *
     * @param name  name of visitor
     * @return
     */
    void setVisitorName(String name);

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    List<SelectedAttraction> getSelectedAttractions();
}

public class Registration implements RegistrationInterface {
    private int registrationId;
    private String visitorName;
    private static List<SelectedAttraction> selectedAttractions = new ArrayList<SelectedAttraction>();

    public Registration(int registrationId, String visitorName, SelectedAttraction selectedAttraction) {
        this.registrationId = registrationId;
        this.visitorName = visitorName;
        this.selectedAttractions.add(selectedAttraction);
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAttractions;
    }
}
