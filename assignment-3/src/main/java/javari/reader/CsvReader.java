package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author William Gates
 */
public class CsvReader {

    public static final String COMMA = ",";

    private int validRecords = 0;
    private int invalidRecords = 0;
    private Path file;
    protected List<String> lines;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CsvReader(Path file, String dataType) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
        countValidAndInvalidRecords(this.lines, dataType);
    }

    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid and invalid records from read CSV file.
     *
     * @return
     */
    public void countValidAndInvalidRecords(List<String> lines, String dataType) {
        if (dataType.equals("attractions")) {
            String[] validAttractions = {"Circles of Fires", "Dancing Animals", "Counting Masters", "Passionate Coders"};
            boolean[] checkedAttractions = new boolean[validAttractions.length];

            for (int i = 0; i < this.lines.size(); i++) {
                boolean flag = false;
                String attraction = this.lines.get(i).split(COMMA)[1];

                for (int j = 0; j < validAttractions.length; j++) {
                    if (attraction.equals(validAttractions[j])) {
                        if (checkedAttractions[j] == false) {
                            validRecords += 1;
                            checkedAttractions[j] = true;
                        }
                        flag = true;
                    }
                }

                if (flag == false) {
                    invalidRecords += 1;
                }
                flag = false;
            }
        }

        else if (dataType.equals("categories")) {
            String[] validCategories = {"mammals", "aves", "reptiles"};
            boolean[] checkedCategories = new boolean[validCategories.length];

            for (int i = 0; i < this.lines.size(); i++) {
                boolean flag = false;
                String category = this.lines.get(i).split(COMMA)[1];

                for (int j = 0; j < validCategories.length; j++) {
                    if (category.equals(validCategories[j])) {
                        if (checkedCategories[j] == false) {
                            validRecords += 1;
                            checkedCategories[j] = true;
                        }
                        flag = true;
                    }
                }

                if (flag == false) {
                    invalidRecords += 1;
                }
                flag = false;
            }
        }

        else if (dataType.equals("sections")) {
            String[] validSections = {"Explore the Mammals", "World of Aves", "Reptillian Kingdom"};
            boolean[] checkedSections = new boolean[validSections.length];

            for (int i = 0; i < this.lines.size(); i++) {
                boolean flag = false;
                String section = this.lines.get(i).split(COMMA)[2];

                for (int j = 0; j < validSections.length; j++) {
                    if (section.equals(validSections[j])) {
                        if (checkedSections[j] == false) {
                            validRecords += 1;
                            checkedSections[j] = true;
                        }
                        flag = true;
                    }
                }

                if (flag == false) {
                    invalidRecords += 1;
                }
                flag = false;
            }
        }

        else if (dataType.equals("records")) {
            for (int i = 0; i < this.lines.size(); i++) {
                int dataCount = 0;
                String record = this.lines.get(i);

                for (int j = 0; j < record.length(); j++) {
                    if (record.substring(j, j + 1).equals(COMMA)) {
                        dataCount += 1;
                    }
                }

                if (dataCount == 7) {
                    validRecords += 1;
                }
                else {
                    invalidRecords += 1;
                }
            }
        }
    }

    /**
     * Returns the valid records from the current file.
     *
     * @return
     */
    public int getValidRecords() {
        return validRecords;
    }

    /**
     * Returns the invalid records from the current file.
     *
     * @return
     */
    public int getInvalidRecords() {
        return invalidRecords;
    }
}
