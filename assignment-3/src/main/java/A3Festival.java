import javari.animal.*;
import javari.menu.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;
import org.json.JSONWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class A3Festival {
    /**
     * Filter the animals that can perform.
     *
     * @param animals   list of animals to be checked whether they can perform or not
     * @param type      type of the animals to be checked
     *
     * @return
     */
    public static ArrayList<Animal> performableAnimals(ArrayList<Animal> animals, String type) {
        ArrayList<Animal> performableAnimalsList = new ArrayList<Animal>();

        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getType().equals(type)) {
                if (animals.get(i).isShowable() == true) {
                    performableAnimalsList.add(animals.get(i));
                }
            }
        }
        return performableAnimalsList;
    }

    /**
     * Check whether the attraction is available in Javari Park.
     *
     * @param type          type of animals to be checked whether their attraction is in Javari Park
     * @param attraction    type of attraction to be checked whether it is in Javari Park
     *
     * @return
     */
    public static boolean checkAttraction(String type, String attraction, List<String> attractionsList) {
        for (int i = 0; i < attractionsList.size(); i++) {
            if (type.equals(attractionsList.get(i).split(",")[0]) &&
                attraction.equals(attractionsList.get(i).split(",")[1])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Main method.
     */
    public static void main(String[] args) {
        // Initialize variables.
        ArrayList<Animal> animals = new ArrayList<Animal>();
        ArrayList<Animal> performableAnimalsList = new ArrayList<Animal>();
        ArrayList<Registration> registrants = new ArrayList<Registration>();
        CsvReader reader;
        List<String> attractions, categories, records;
        attractions = categories = records = new ArrayList<String>();
        Scanner input = new Scanner(System.in);
        SelectedAttraction selector = null;

        boolean attractionAvailability, isEnd;
        attractionAvailability = isEnd = false;
        int id = 1;
        int validSections, validAttractions, validCategories, validRecords;
        validSections = validAttractions = validCategories = validRecords = 0;
        int invalidSections, invalidAttractions, invalidCategories, invalidRecords;
        invalidSections = invalidAttractions = invalidCategories = invalidRecords = 0;
        String dataPath = "./data";
        String[] command = new String[6];
        command[5] = "Y";

        // Ask for file path input from user.
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();
        for (int i = 0; i < 2; i++) {
            if (i == 1) {
                System.out.println();
                System.out.print("Please provide the source data path: ");
                dataPath = input.nextLine();
                System.out.println();
            }

            // Read and detect valid and invalid records form the file.
            try {
                reader = new CsvReader(Paths.get(dataPath + "/animals_categories.csv"), "sections");
                categories = reader.getLines();
                validSections = reader.getValidRecords();
                invalidSections = reader.getInvalidRecords();

                reader = new CsvReader(Paths.get(dataPath + "/animals_attractions.csv"), "attractions");
                attractions = reader.getLines();
                validAttractions = reader.getValidRecords();
                invalidAttractions = reader.getInvalidRecords();

                reader = new CsvReader(Paths.get(dataPath + "/animals_categories.csv"), "categories");
                categories = reader.getLines();
                validCategories = reader.getValidRecords();
                invalidCategories = reader.getInvalidRecords();

                reader = new CsvReader(Paths.get(dataPath + "/animals_records.csv"), "records");
                records = reader.getLines();
                validRecords = reader.getValidRecords();
                invalidRecords = reader.getInvalidRecords();
                break;
            }
            catch (IOException e) {
                System.out.println("... Opening default section database from data. ... File not found or incorrect file!");
            }
        }

        // Add all animals in records to a list.
        for (int i = 0; i < records.size(); i++) {
            String record[] = records.get(i).split(",");

            animals.add(new Animal(Integer.parseInt(record[0]),
                                   record[1],
                                   record[2],
                                   Gender.parseGender(record[3]),
                                   Double.parseDouble(record[4]),
                                   Double.parseDouble(record[5]),
                                   record[6],
                                   Condition.parseCondition(record[7])));
        }

        // Print the valid and invalid records from the file.
        System.out.println("... Loading... Success... System is populating data...");
        System.out.println();
        System.out.println("Found " + validSections + " valid sections and " + invalidSections + " invalid sections");
        System.out.println("Found " + validAttractions + " valid attractions and " + invalidAttractions + " invalid attractions");
        System.out.println("Found " + validCategories + " valid categories and " + invalidCategories + " invalid categories");
        System.out.println("Found " + validRecords + " valid records and " + invalidRecords + " invalid records");
        System.out.println();
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");
        System.out.println();

        // Ask visitor for input as to what they will do.
        while (command[5].equals("Y")) {
            attractionAvailability = false;
            performableAnimalsList = null;
            selector = null;

            // Print the Javari Park sections.
            Menu.printMainMenu();
            command[0] = input.nextLine();
            System.out.println();

            switch(command[0]) {
                // Print the list of mammals.
                case "1":
                    Menu.printMammalsList();
                    command[1] = input.nextLine();
                    System.out.println();

                    switch(command[1]) {
                        // Print the list of hamster's attractions and ask for input.
                        case "1":
                            performableAnimalsList = performableAnimals(animals, "Hamster");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printHamsterAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Hamster", "Dancing Animals", attractions);
                                        selector = new SelectedAttraction("Dancing Animals", "Hamster", performableAnimalsList);
                                        break;
                                    case "2":
                                        attractionAvailability = checkAttraction("Hamster", "Counting Masters", attractions);
                                        selector = new SelectedAttraction("Counting Masters", "Hamster", performableAnimalsList);
                                        break;
                                    case "3":
                                        attractionAvailability = checkAttraction("Hamster", "Passionate Coders", attractions);
                                        selector = new SelectedAttraction("Passionate Coders", "Hamster", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
                            }
                            break;
                        // Print the list of lion's attractions and ask for input.
                        case "2":
                            performableAnimalsList = performableAnimals(animals, "Lion");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printLionAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Lion", "Circles of Fires", attractions);
                                        selector = new SelectedAttraction("Circles of Fires", "Lion", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no lion can perform any attraction, please choose other animals");
                            }
                            break;
                        // Print the list of cat's attractions and ask for input.
                        case "3":
                            performableAnimalsList = performableAnimals(animals, "Cat");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printCatAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Cat", "Dancing Animals", attractions);
                                        selector = new SelectedAttraction("Dancing Animals", "Cat", performableAnimalsList);
                                        break;
                                    case "2":
                                        attractionAvailability = checkAttraction("Cat", "Passionate Coders", attractions);
                                        selector = new SelectedAttraction("Passionate Coders", "Cat", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no cat can perform any attraction, please choose other animals");
                            }
                            break;
                        // Print the list of whale's attractions and ask for input.
                        case "4":
                            performableAnimalsList = performableAnimals(animals, "Whale");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printWhaleAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Whale", "Circles of Fires", attractions);
                                        selector = new SelectedAttraction("Circles of Fires", "Whale", performableAnimalsList);
                                        break;
                                    case "2":
                                        attractionAvailability = checkAttraction("Whale", "Counting Masters", attractions);
                                        selector = new SelectedAttraction("Counting Masters", "Whale", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no whale can perform any attraction, please choose other animals");
                            }
                            break;
                        case "#":
                            System.out.println();
                            break;
                        default:
                            System.out.println("Wrong input");
                            System.out.println();
                            break;
                    }
                    break;

                // Print the list of aves.
                case "2":
                    Menu.printAvesList();
                    command[1] = input.nextLine();
                    System.out.println();
                    switch(command[1]) {
                        // Print the list of eagle's attractions and ask for input.
                        case "1":
                            performableAnimalsList = performableAnimals(animals, "Eagle");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printEagleAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Eagle", "Circles of Fires", attractions);
                                        selector = new SelectedAttraction("Circles of Fires", "Eagle", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no eagle can perform any attraction, please choose other animals");
                            }
                            break;
                        // Print the list of parrot's attractions and ask for input.
                        case "2":
                            performableAnimalsList = performableAnimals(animals, "Parrot");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printParrotAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Parrot", "Dancing Animals", attractions);
                                        selector = new SelectedAttraction("Dancing Animals", "Parrot", performableAnimalsList);
                                        break;
                                    case "2":
                                        attractionAvailability = checkAttraction("Parrot", "Counting Masters", attractions);
                                        selector = new SelectedAttraction("Counting Masters", "Parrot", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no parrot can perform any attraction, please choose other animals");
                            }
                        case "#":
                            System.out.println();
                            break;
                        default:
                            System.out.println("Wrong input");
                            System.out.println();
                            break;
                    }
                    break;

                // Print the list of reptiles.
                case "3":
                    Menu.printReptilesList();
                    command[1] = input.nextLine();
                    System.out.println();
                    switch (command[1]) {
                        // Print the list of snake's attractions and ask for input.
                        case "1":
                            performableAnimalsList = performableAnimals(animals, "Snake");
                            if (performableAnimalsList.size() > 0) {
                                Menu.printSnakeAttractions();
                                command[2] = input.nextLine();
                                System.out.println();

                                switch (command[2]) {
                                    case "1":
                                        attractionAvailability = checkAttraction("Snake", "Dancing Animals", attractions);
                                        selector = new SelectedAttraction("Dancing Animals", "Snake", performableAnimalsList);
                                        break;
                                    case "2":
                                        attractionAvailability = checkAttraction("Snake", "Passionate Coders", attractions);
                                        selector = new SelectedAttraction("Passionate Coders", "Snake", performableAnimalsList);
                                        break;
                                    case "#":
                                        System.out.println();
                                        break;
                                    default:
                                        System.out.println("Wrong input");
                                        System.out.println();
                                        break;
                                }
                            }
                            else {
                                System.out.println("Unfortunately, no snake can perform any attraction, please choose other animals");
                            }
                            break;
                        case "#":
                            System.out.println();
                            break;
                        default:
                            System.out.println("Wrong input");
                            System.out.println();
                            break;
                    }
                    break;

                default:
                    System.out.println("Wrong input");
                    System.out.println();
            }

            // Data checkout for visitor.
            if ((performableAnimalsList.size() > 0) && (attractionAvailability == true)) {
                System.out.println("Wow, one more step,");
                System.out.print("please let us know your name: ");
                command[3] = input.nextLine();
                System.out.println();
                System.out.println("Yeay, final check!");
                System.out.println("Here is your data, and the attraction you chose:");
                System.out.println("Name: " + command[3]);
                System.out.println("Attractions: " + selector.getName() + " -> " + selector.getType());
                System.out.print("With: ");

                for (int i = 0; i < performableAnimalsList.size(); i++) {
                    System.out.print(performableAnimalsList.get(i).getName());
                    if (i != performableAnimalsList.size() - 1) {
                        System.out.print(", ");
                    }
                }

                System.out.println();
                System.out.println();
                System.out.print("Is the data correct? (Y/N): ");
                command[4] = input.nextLine();
                System.out.println();

                if (command[4].equals("Y")) {
                    registrants.add(new Registration(id++, command[3], selector));
                    System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
                    command[5] = input.nextLine();
                    System.out.println();
                }
            }
            else if ((performableAnimalsList.size() > 0) && (attractionAvailability == false)) {
                System.out.println("Unfortunately, the attraction is not performable, please choose other animals");
                System.out.println();
            }
        }

        // Write the checkout to .json file.
        try {
            System.out.print("... End of program, write to ");
            for (int i = 0; i < registrants.size(); i++) {
                RegistrationWriter.writeJson(registrants.get(i), Paths.get("."));
                if (i != registrants.size() - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
        catch (IOException e) {
            System.out.println("Error");
        }
    }
}
