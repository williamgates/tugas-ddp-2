package bar;

import board.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * This class handles the "Play Again" and "Exit" buttons. The "Play Again" button is used to
 * restart the game. The "Exit" button is used to close the application.
 *
 * @author William Gates
 */
public class Menu extends JPanel {
    private Board board;
    private JButton playAgain, exit;
    private JSplitPane buttonsSplit;

    /**
     * Constructor.
     *
     * @param board     board of the game that consist of cells.
     */
    public Menu(Board board) {
        this.board = board;
        playAgain = new JButton("Play Again");
        exit = new JButton("Exit");
        buttonsSplit = new JSplitPane();

        playAgain.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                board.newGame();
            }
        });

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        buttonsSplit.setLeftComponent(playAgain);
        buttonsSplit.setRightComponent(exit);

        add(buttonsSplit);
    }
}
