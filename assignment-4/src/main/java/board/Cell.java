package board;

import bar.*;

import java.awt.*;
import javax.swing.*;

/**
 * This class handles the image and ID of a cell. The ID will then be used to check whether the
 * selected cells have the same ID.
 *
 * @author William Gates
 */
public class Cell extends JButton {
    private static ImageIcon unselectedIcon = new ImageIcon("./img/img0.jpg");

    private int id;
    private ImageIcon icon;

    /**
     * Constructor.
     *
     * @param id        the ID of the cell.
     */
    public Cell(int id) {
        this.id = id;
        icon = new ImageIcon("./img/img" + id + ".jpg");

        setBackground(Color.WHITE);
        setOpaque(true);
        unselected();
    }

    /**
     * This method is used to set the cell to invisible and disbled.
     */
    public void matched(){
        setEnabled(false);
        setVisible(false);
    }

    /**
     * This method is used to show the image of the selected cell.
     */
    public void selected() {
        setIcon(icon);
    }

    /**
     * This method is used to hide the image of the cell.
     */
    public void unselected() {
        setIcon(unselectedIcon);
    }

    /**
     * This is the getter method for id.
     *
     * @return id       the ID of the cell.
     */
    public int getId() {
        return id;
    }
}
