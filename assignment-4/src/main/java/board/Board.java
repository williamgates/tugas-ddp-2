package board;

import bar.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.*;

/**
 * This class handles the board of the game that consists of 18 pairs of randomly placed
 * images/cells.
 *
 * @author William Gates
 */
public class Board extends JPanel {
    private int pairs;
    private Cell firstCell, secondCell, selectedCell;
    private List<Cell> cells;
    private JLabel winText;
    private Status status;
    private Timer timer;

    /**
     * Constructor.
     *
     * @param status    the status that handles the number of tries.
     */
    public Board(Status status) {
        this.status = status;
        init();
    }

    /**
     * This method is used to check whether the first cell and the second cell that the player has
     * selected has the same ID.
     */
    public void checkSelectedCells() {
        if (firstCell.getId() == secondCell.getId()) {
            firstCell.matched();
            secondCell.matched();
            pairs -= 1;
            checkWin();
        }
        else {
            firstCell.unselected();
            secondCell.unselected();
        }

        status.increment();
        firstCell = null;
        secondCell = null;
    }

    /**
     * This method is used to to check whether the player has won and display a winning message if
     * the player has won.
     */
    public void checkWin() {
        if (pairs == 0) {
            deleteBoard();
            setLayout(new GridLayout(1, 1));
            add(winText);
        }
    }

    /**
     * This method is used to delete all cells and winning message in the board.
     */
    public void deleteBoard() {
        for (int i = 0; i < cells.size(); i++) {
            remove(cells.get(i));
        }
        remove(winText);
        revalidate();
        repaint();
    }

    /**
     * This method is used to initialize the board with 18 pairs of randomly placed images/cells.
     * Each images/cells will be placed in 6x6 grid and a pair of images/cells will be invisible
     * and disabled if the player has selected that correct pair.
     */
    public void init() {
        pairs = 18;
        cells = new ArrayList<Cell>();
        winText = new JLabel("You win!", SwingConstants.CENTER);
        winText.setFont(new Font("", Font.PLAIN, 64));
        timer = new Timer(500, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkSelectedCells();
            }
        });

        timer.setRepeats(false);
        status.setCount(0);

        for (int id = 1; id <= pairs; id++) {
            cells.add(new Cell(id));
            cells.add(new Cell(id));
        }

        Collections.shuffle(cells);
        setLayout(new GridLayout(6, 6));

        for (int i = 0; i < cells.size(); i++) {
            final Cell selected = cells.get(i);

            cells.get(i).addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    selectedCell = selected;

                    if (firstCell == null && secondCell == null) {
                        firstCell = selectedCell;
                        selectedCell.selected();
                    }
                    if (firstCell != null && secondCell == null && selectedCell != firstCell) {
                        secondCell = selectedCell;
                        selectedCell.selected();
                        timer.start();
                    }
                }
            });

            add(cells.get(i));
        }
    }

    /**
     * This method is used to create a new game that will delete the board and reinitialize the
     * board.
     */
    public void newGame() {
        deleteBoard();
        init();
    }
}
