import board.*;
import bar.*;

import java.awt.*;
import javax.swing.*;

/**
 * This class handles the frame of the application and the position of every panels inside the
 * frame.
 *
 * @author William Gates
 */
public class Game extends JFrame {
    private board.Board board;
    private bar.Status status;
    private bar.Menu menubar;

    /**
     * Constructor.
     */
    public Game() {
        super();

        status = new bar.Status();
        board = new board.Board(status);
        menubar = new bar.Menu(board);

        add(board, BorderLayout.CENTER);
        add(status, BorderLayout.SOUTH);
        add(menubar, BorderLayout.NORTH);
        setBackground(Color.WHITE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setResizable(false);
        setTitle("Match Pairs Memory Game");
        setVisible(true);

        pack();
    }

    /**
     * Main method.
     */
    public static void main(String[] args) {
        new Game();
    }
}
